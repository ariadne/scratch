package main

import (
	"fmt"
	"log"
	"net/url"
	"strings"

	"github.com/go-git/go-git/v5"
)

func main() {
	repo, err := git.PlainOpen(".")
	if err != nil {
		log.Fatal(err)
	}

	remote, err := repo.Remote("origin")
	if err != nil {
		log.Fatal(err)
	}

	remoteConfig := remote.Config()
	remoteURL := remoteConfig.URLs[0]

	log.Printf("remote = %s", remoteURL)

	normalizedURL, err := url.Parse(remoteURL)
	if err != nil {
		log.Printf("URL is a git+ssh:// type URL")

		remoteURL = strings.Replace(remoteURL, ":", "/", 1)
		normalizedURL, err = url.Parse(fmt.Sprintf("git+ssh://%s", remoteURL))
		if err != nil {
			log.Fatal(err)
		}
	}

	log.Printf("normalized url = %s", normalizedURL)
}
